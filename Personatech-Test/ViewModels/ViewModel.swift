//
//  ViewModel.swift
//  Personatech-Test
//
//  Created by Apple on 25/06/24.
//

import Foundation
import SwiftUI

class ViewModel: ObservableObject {
    
    @Published private(set) var productListArray: [ProductListModel] = []
    @Published private(set) var addedProductListArray:[ProductListModel] = []
    
    @Published var searchProductsQuery: String = ""
    @Published private(set) var errorMessage: String = ""
    
    @Published var isEditing: Bool = false
    @Published private(set) var isListEmpty: Bool = false
    @Published private(set) var keyboardIsShown: Bool = false
    
    var filterProductListArray: [ProductListModel] = []
    var sectionListArray: [SectionModel] = [SectionModel(name: "Available")]
    var totalProductsInCart: [[Int:Int]] = []
    var totalCartValue: Int = 0
    var totalDiscountValue: Int = 0
    var totalCartAmount: Int = 0
    
    let networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func fetchProductList() async {
        do {
            let productArray = try await self.networkManager.makeApiRequest(url: AppConstant.baseUrl, response: [ProductListModel].self)
            await MainActor.run {
                if productArray.count == .zero {
                    self.isListEmpty = true
                }else {
                    self.productListArray = productArray
                    self.filterProductListArray = self.productListArray
                }
            }
        }catch {
            await MainActor.run {
                errorMessage = "Products list is not available !!"
            }
        }
    }
    
    func addProductInAddedList(_ product: ProductListModel) {
        withAnimation(.easeInOut(duration: 0.4)) {
            if addedProductListArray.isEmpty {
                sectionListArray.insert(SectionModel(name: "Added"), at: 0)
            }
            
            if !addedProductListArray.contains(where: { currentProduct in
                currentProduct.productID == product.productID
            }) {
                addedProductListArray.append(product)
                updateCartAmountValues()
            }
        }
    }
    
    func updateAddedProductQuantity(_ type: QuantityType, product: ProductListModel, index: Int) {
        switch type {
        case .increment:
            addedProductListArray[index].totalCarAmount += 1
        case .decrement:
            addedProductListArray[index].totalCarAmount = addedProductListArray[index].totalCarAmount == 1 ? 1 : addedProductListArray[index].totalCarAmount - 1
        }
        updateCartAmountValues()
    }
    
    func removeAddedProduct(_ index: Int) {
        withAnimation(.easeInOut(duration: 0.4)) {
            addedProductListArray.remove(at: index)
            if addedProductListArray.isEmpty {
                sectionListArray.removeFirst()
            }
            updateCartAmountValues()
        }
    }
    
    func updateCartAmountValues() {
        totalCartValue = addedProductListArray.compactMap { $0.returnTotalCarAmount }.reduce(0, +)
        totalDiscountValue = addedProductListArray.compactMap { $0.returnTotalCartDiscount }.reduce(0, +)
        totalCartAmount = totalCartValue - totalDiscountValue
    }
    
    func updateKeyboardStatus(_ isShown: Bool) {
        keyboardIsShown = isShown
    }
    
    func clearSearchQueryText() {
        searchProductsQuery = ""
    }
    
    func filterProductsByName(_ searchProductText: String) {
        let arrayStatus = self.filterProductListArray.filter { $0.name.lowercased().contains(searchProductText.lowercased())}
        self.productListArray =  searchProductText.isEmpty ? self.filterProductListArray : arrayStatus.isEmpty ? [ProductListModel(productID: 10, name: "", price: 0.0, image: "", discount: 0)] : arrayStatus
    }
}
