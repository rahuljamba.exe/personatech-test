//
//  NetworkManager.swift
//  Personatech-Test
//
//  Created by Apple on 25/06/24.
//

import Foundation

actor NetworkManager {
    func makeApiRequest<T: Decodable>(url: String, response: T.Type) async throws -> T {
        
        guard let inputUrl = URL(string: url) else {
            throw NetworkManagerError.invalidUrl
        }
        
        let (data, _) = try await URLSession.shared.data(from: inputUrl)
        let decodableData = try JSONDecoder().decode(response.self, from: data)
        return decodableData
    }
}
