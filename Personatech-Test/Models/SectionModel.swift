//
//  SectionModel.swift
//  Personatech-Test
//
//  Created by Apple on 26/06/24.
//

import Foundation

struct SectionModel : Identifiable {
    let id = UUID()
    let name: String
}
