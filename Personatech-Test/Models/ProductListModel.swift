//
//  ProductListModel.swift
//  Personatech-Test
//
//  Created by Apple on 25/06/24.
//

import Foundation

// MARK: - ProductListModel
struct ProductListModel: Decodable {
    let productID: Int
    let name: String
    let price: Double
    let image: String
    let discount: Int
    
    var totalCarAmount: Int = 1
    
    var returnTotalCarAmount: Int {
        Int(price) * totalCarAmount
    }
    
    var returnTotalCartDiscount: Int {
        Int(discount) * totalCarAmount
    }
    
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case name, price, image, discount
    }
}
