//
//  Personatech_TestApp.swift
//  Personatech-Test
//
//  Created by Apple on 25/06/24.
//

import SwiftUI

@main
struct Personatech_TestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}


