//
//  ContentView.swift
//  Personatech-Test
//
//  Created by Apple on 25/06/24.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject private var viewModel = ViewModel(networkManager: NetworkManager())
    @FocusState var isTextFieldFocused: Bool
    
    @State var scroll: Bool = false
    
    var body: some View {
        ZStack {
            VStack {
                
                HeaderView()
                
                if !viewModel.errorMessage.isEmpty {
                    Spacer()
                    Text(viewModel.errorMessage)
                        .lineLimit(5)
                        .font(.largeTitle)
                    Spacer()
                } else if viewModel.isListEmpty {
                    Spacer()
                      AddTextView("Product list is empty, Please add some products.!")
                    Spacer()
                } else {
                    if viewModel.productListArray.isEmpty {
                        SpinnerLoaderView()
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                    } else {
                        ScrollViewReader { proxy in
                            List {
                               Section {
                                    CompanyInformationView()
                                        .id(100)
                                        .listRowInsets(EdgeInsets(top: -15, leading: -15, bottom: 0, trailing: -15))
                                        .listRowBackground(Color.clear)
                                        .padding()
                                    
                                    SearchBarTextfieldView(isTextFieldFocused: _isTextFieldFocused, vm: viewModel)
                                        .id(200)
                                        .frame(height: 100)
                                        .listRowInsets(EdgeInsets(top: viewModel.isEditing ? -30 : -50, leading: -15, bottom: 0, trailing: -15))
                                        .listRowBackground(Color.clear)
                                        .listRowSeparator(.hidden)
                                        .padding()
                                        .animation(.smooth, value: viewModel.isEditing)
                                        .onChange(of: viewModel.searchProductsQuery) { _, newValue in
                                            viewModel.filterProductsByName(newValue)
                                        }
                                    
                                    if viewModel.addedProductListArray.isEmpty && !viewModel.isEditing {
                                        AddTextView("No Products added to the cart")
                                            .padding(.top, -30)
                                            .listRowBackground(Color.clear)
                                            .listRowSeparator(.hidden)
                                    }
                                }
                                
                                ForEach(viewModel.sectionListArray) { sectionTitle in
                                    if sectionTitle.name == "Added" && !viewModel.isEditing {
                                        RoundedSectionView(header:
                                                            Text("Added")
                                            .font(.title2).bold()
                                            .foregroundStyle(Color.secondary)
                                        ) {
                                            ForEach(Array(viewModel.addedProductListArray.enumerated()), id: \.offset) { index, addedProductItem in
                                                SetUpAddedProductCell(addedProductItem, index)
                                                    .listRowSeparator(.hidden)
                                            }
                                        }
                                        .listRowSeparator(.hidden)
                                        .listRowInsets(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
                                        .padding(.top, -80)
                                    }
                                    
                                    if sectionTitle.name == "Available" {
                                        if viewModel.productListArray.first?.productID == 10  {
                                            AddTextView("This product is not available !!")
                                                .listRowSeparator(.hidden)
                                        }else {
                                            RoundedSectionView(header:
                                             Text("Available")
                                                .font(.title2).bold()
                                                .foregroundStyle(Color.secondary)
                                            ) {
                                                ForEach(viewModel.productListArray, id: \.productID) { productItem in
                                                    SetUpAvailableProductCell(productItem)
                                                        .listRowSeparator(.hidden)
                                                }
                                            }
                                            .listRowSeparator(.hidden)
                                            .listRowInsets(EdgeInsets(top: viewModel.isEditing ? -80 : 0, leading: 15, bottom: 0, trailing: 15))
                                        }
                                    }
                                }
                            }
                            .listStyle(.plain)
                            .onChange(of: viewModel.isEditing) { old, newValue in
                                DispatchQueue.main.async {
                                    withAnimation {
                                        if newValue {
                                            proxy.scrollTo(200, anchor: .top)
                                        }else {
                                            if viewModel.productListArray.first?.productID == 10 {
                                                viewModel.clearSearchQueryText()
                                            }
                                            proxy.scrollTo(100, anchor: .top)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            // Background view to handle taps
            Color.clear
                .contentShape(Rectangle())
                .onTapGesture {
                    if viewModel.keyboardIsShown {
                        isTextFieldFocused = false
                    }
                }
                .allowsHitTesting(viewModel.keyboardIsShown)
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.white)
        .ignoresSafeArea()
        .onReceive(NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)) { _ in
            viewModel.updateKeyboardStatus(true)
        }
        .onReceive(NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)) { _ in
            viewModel.updateKeyboardStatus(false)
        }
        .task {
            await viewModel.fetchProductList()
        }
    }
    
    @ViewBuilder
    func HeaderView() -> some View {
        Rectangle()
            .frame(maxWidth: .infinity)
            .frame(height: 100)
            .overlay(
                Text("Cart")
                    .font(.headline)
                    .foregroundColor(.white)
                    .offset(y: 15)
            )
    }
    
    @ViewBuilder
    func CompanyInformationView() -> some View {
        VStack{
            HStack(alignment: .top, spacing: 20) {
                Image(.boatLogo)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 50, height: 50)
                    .clipShape(Circle())
                    .padding(.leading, 0)
                
                VStack(alignment: .leading, spacing: 10) {
                    Text("boAt Lifestyle")
                        .font(.headline).bold()
                    
                    Text("15KM distance from your current location")
                        .lineLimit(5)
                        .font(.system(size: 14))
                        .foregroundStyle(Color.secondary)
                    
                    HStack {
                        Text("Cart Value: \(viewModel.totalCartValue)")
                        Text("Discount : \(viewModel.totalDiscountValue)")
                        Text("Total: \(viewModel.totalCartAmount)")
                    }
                    .font(.system(size: 10, weight: .bold))
                    .animation(.smooth, value: viewModel.totalCartValue)
                    .monospacedDigit()
                }
            }
            .frame(maxWidth: .infinity)
            .frame(height: 130)
            .background(Color.white)
            .cornerRadius(10)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.black, lineWidth: 2)
            )
            .padding()
        }
    }
    
    @ViewBuilder
    func AddTextView( _ text:  String) -> some View {
        Text(text)
            .lineLimit(5)
            .font(.title2).bold()
            .foregroundStyle(Color.gray)
            .frame(maxWidth: .infinity, alignment: .center)
            .background( viewModel.isEditing ? Color.clear : Color.white)
        
    }
    
    @ViewBuilder
    func SetUpAddedProductCell(_ productItem: ProductListModel,_ index: Int) -> some View {
        HStack(alignment: .center) {
            AsyncImage(
                url: URL(string: productItem.image),
                transaction: Transaction(animation: .default),
                content: { phase in
                    if let image = phase.image {
                        image
                            .resizable()
                    }
                    else {
                        Image(.boatLogo)
                            .resizable()
                    }
                }
            )
            .scaledToFill()
            .frame(width: 50, height: 50)
            .clipShape(Circle())
            
            VStack(alignment: .leading, spacing: 10) {
                Text(productItem.name.description).bold()
                Text("Price ").font(.caption) +  Text(productItem.price, format: .currency(code: "USD")).font(.caption)
                Text("Discount ").font(.caption).font(.caption2) +  Text(productItem.discount, format: .currency(code: "USD")).font(.caption2)
            }.padding(.leading, 10)
            
            
            VStack(alignment: .center, spacing: 10) {
                Text("QT : \(viewModel.addedProductListArray[index].totalCarAmount)")
                    .frame(maxWidth: .infinity)
                    .font(.headline).bold()
                    .monospacedDigit()
                
                Stepper ("") {
                    viewModel.updateAddedProductQuantity(.increment, product: productItem, index: index)
                } onDecrement: {
                    viewModel.updateAddedProductQuantity(.decrement, product: productItem, index: index)
                }
                .labelsHidden()
                
                Image(systemName: "minus.circle.fill")
                    .foregroundStyle(Color.red)
                    .font(.system(size: 30))
                    .onTapGesture {
                        viewModel.removeAddedProduct(index)
                    }
            }
        }
        .padding(.bottom, 20)
    }
    
    @ViewBuilder
    func SetUpAvailableProductCell(_ productItem: ProductListModel)  -> some View {
        HStack(alignment: .center) {
            AsyncImage(
                url: URL(string: productItem.image),
                transaction: Transaction(animation: .default),
                content: { phase in
                    if let image = phase.image {
                        image
                            .resizable()
                    }
                    else {
                        Image(.boatLogo)
                            .resizable()
                    }
                }
            )
            .scaledToFill()
            .frame(width: 50, height: 50)
            .clipShape(Circle())
            
            VStack(alignment: .leading, spacing: 10) {
                Text(productItem.name).bold()
                Text("Price ") +  Text(productItem.price, format: .currency(code: "USD"))
                Text("Discount ").font(.caption) +  Text(productItem.discount, format: .currency(code: "USD")).font(.caption)
            }.padding(.leading, 10)
            
            Spacer()
            
            Image(systemName: "plus.circle.fill")
                .foregroundStyle(Color.green)
                .font(.system(size: 30))
                .onTapGesture {
                    viewModel.addProductInAddedList(productItem)
                }
        }
        .padding(.bottom, 20)
    }
}

#Preview {
    ContentView()
}
