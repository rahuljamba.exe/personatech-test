//
//  RoundedSectionView.swift
//  Personatech-Test
//
//  Created by Apple on 02/07/24.
//

import SwiftUI

struct RoundedSectionView<Content: View, Header: View>: View {
    let content: Content
    let header: Header
    
    init(header: Header, @ViewBuilder content: () -> Content) {
        self.content = content()
        self.header = header
    }
    
    var body: some View {
        Section(header: header) {
            VStack(spacing: 0) {
                content
            }
            .padding()
            .background(Color(.systemBackground))
            .clipShape(RoundedRectangle(cornerRadius: 10))
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.gray.opacity(0.2), lineWidth: 1)
            )
            .padding(.vertical, 8)
        }
    }
}
