//
//  SearchBarTextfieldView.swift
//  Personatech-Test
//
//  Created by Apple on 29/06/24.
//

import SwiftUI

struct SearchBarTextfieldView: View {

      @FocusState var isTextFieldFocused: Bool
    
      @ObservedObject var vm: ViewModel
     
    var body: some View {
        HStack {
            
            TextField("Search product...", text: $vm.searchProductsQuery, onEditingChanged: { status in
                withAnimation(.smooth) {
                    vm.isEditing = status
                }
            })
            .focused($isTextFieldFocused)
            .font(.system(size: 18).bold())
            .frame(maxHeight: .infinity)
            .padding(.leading, 8)
            .padding(.trailing, 8)
                
            if !vm.searchProductsQuery.isEmpty {
                Button(action: {
                    withAnimation(.smooth) {
                        vm.searchProductsQuery = ""
                        isTextFieldFocused = false
                    }
                }) {
                    Image(systemName: "xmark.circle.fill")
                        .foregroundColor(Color.secondary)
                        .padding(.trailing, 10)
                }
            }
        }
        .frame(height: 50)
        .background(Color.gray.opacity(0.5))
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color.black, lineWidth: 1)
        )
        .padding()
    }
}
