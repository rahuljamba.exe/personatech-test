//
//  SpinnerLoaderView.swift
//  Personatech-Test
//
//  Created by Apple on 29/06/24.
//

import SwiftUI

struct SpinnerLoaderView: View {
    var body: some View {
        VStack(spacing: 10) {
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: .gray))
                .scaleEffect(2.0, anchor: .center)
            Text("Loading...")
                .font(.title2)
                .foregroundColor(.gray)
                .padding(.top, 8)
        }
    }
}

#Preview {
    SpinnerLoaderView()
}
