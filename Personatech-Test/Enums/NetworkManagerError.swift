//
//  NetworkManagerError.swift
//  Personatech-Test
//
//  Created by Apple on 29/06/24.
//

import Foundation

enum NetworkManagerError: Error {
    case invalidUrl
}
